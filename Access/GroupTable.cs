﻿using Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using System.Threading.Tasks;


namespace Access
{
    public class GroupTable
    {
        private readonly string _connectionString;
        public GroupTable()
        {
            _connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Султановак\source\repos\Homework1\Access\DBGroup.mdf;Integrated Security=True";
        }

        public void AddTable()
        {
            string query = @"create table groupTable (Id int not null, Name nvarchar(50) not null)";
            using (var connection = new SqlConnection(_connectionString))
            using (SqlCommand command = new SqlCommand(query, connection))
            {
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}
